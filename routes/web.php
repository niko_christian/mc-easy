<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// auth route
Auth::routes();

Route::get('/', 'HomeController@landingpage');



//home user
Route::get('/home', 'HomeController@index');

//home admin
Route::get('/admin', 'AdminController@index')->name('admin')->middleware('auth', 'admin');


//Karyawan dashboard admin 
Route::get('/admin/karyawan', 'KaryawanController@index')->middleware('auth', 'admin');
Route::get('/admin/karyawan/tambah', 'KaryawanController@karyawantambah')->middleware('auth', 'admin');
Route::post('/admin/karyawan/store', 'KaryawanController@karyawanstore')->middleware('auth', 'admin');
Route::get('/admin/karyawan/edit/{id}', 'KaryawanController@karyawanedit')->middleware('auth', 'admin');
Route::put('/admin/karyawan/update/{id}', 'KaryawanController@karyawanupdate')->middleware('auth', 'admin');
Route::get('/admin/karyawan/hapus/{id}', 'KaryawanController@karyawandelete')->middleware('auth', 'admin');

//Cuti dashboard admin 
Route::get('/admin/cuti', 'CutiController@index')->middleware('auth', 'admin');
Route::get('/admin/cuti/tambah/{id}', 'CutiController@cutitambah')->middleware('auth', 'admin');
Route::post('/admin/cuti/store', 'CutiController@cutistore')->middleware('auth', 'admin');
Route::get('/admin/cuti/edit/{id}', 'CutiController@cutiedit')->middleware('auth', 'admin');
Route::put('/admin/cuti/update/{id}', 'CutiController@cutiupdate')->middleware('auth', 'admin');
Route::get('/admin/cuti/hapus/{id}', 'CutiController@cutidelete')->middleware('auth', 'admin');


Route::get('/admin/sisacuti/{e}', 'CutiController@sisa')->middleware('auth', 'admin');
Route::get('/admin/lebihcuti', 'CutiController@lebih')->middleware('auth', 'admin');