<?php

use Illuminate\Database\Seeder;

class CutiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cuti')->insert([
        	'Nomor_induk' => 'IP06001',
        	'Tanggal_cuti' => '2020-08-02',
            'Lama_cuti' => 2,
            'Keterangan' => 'Acara Keluarga',
            'created_at'=> now()
        ]);

        DB::table('cuti')->insert([
        	'Nomor_induk' => 'IP06001',
        	'Tanggal_cuti' => '2020-08-18',
            'Lama_cuti' => 2,
            'Keterangan' => 'Anak Sakit',
            'created_at'=> now()
        ]);

        DB::table('cuti')->insert([
        	'Nomor_induk' => 'IP06006',
        	'Tanggal_cuti' => '2020-08-19',
            'Lama_cuti' => 1,
            'Keterangan' => 'Nenek Sakit',
            'created_at'=> now()
        ]);

        DB::table('cuti')->insert([
        	'Nomor_induk' => 'IP06007',
        	'Tanggal_cuti' => '2020-08-23',
            'Lama_cuti' => 1,
            'Keterangan' => 'Sakit',
            'created_at'=> now()
        ]);
        DB::table('cuti')->insert([
        	'Nomor_induk' => 'IP06004',
        	'Tanggal_cuti' => '2020-08-29',
            'Lama_cuti' => 5,
            'Keterangan' => 'Menikah',
            'created_at'=> now()
        ]);
        DB::table('cuti')->insert([
        	'Nomor_induk' => 'IP06003',
        	'Tanggal_cuti' => '2020-08-30',
            'Lama_cuti' => 2,
            'Keterangan' => 'Acara Keluarga',
            'created_at'=> now()
        ]);
    }
}
