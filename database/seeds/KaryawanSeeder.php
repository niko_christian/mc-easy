<?php

use Illuminate\Database\Seeder;

class KaryawanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // insert data ke table pegawai
        DB::table('karyawan')->insert([
        	'Nomor_induk' => 'IP06001',
        	'Nama' => 'Agus',
        	'Alamat' => 'Jln Gaja Mada no 12, Surabaya',
        	'Tanggal_lahir' => '1980-01-11',
            'Tanggal_bergabung' => '2005-08-07',
            'created_at'=> now()
        ]);
        DB::table('karyawan')->insert([
        	'Nomor_induk' => 'IP06002',
        	'Nama' => 'Amin',
        	'Alamat' => 'Jln Imam Bonjol no 11, Mojokerto',
        	'Tanggal_lahir' => '1977-09-03',
            'Tanggal_bergabung' => '2005-08-07',
            'created_at'=> now()
        ]);
        DB::table('karyawan')->insert([
        	'Nomor_induk' => 'IP06003',
        	'Nama' => 'Yusuf',
        	'Alamat' => 'Jln A Yani Raya 15 No 14, Malang',
        	'Tanggal_lahir' => '1973-08-09',
            'Tanggal_bergabung' => '2006-08-07',
            'created_at'=> now()
        ]);
        DB::table('karyawan')->insert([
        	'Nomor_induk' => 'IP06004',
        	'Nama' => 'Alysaa',
        	'Alamat' => 'Jln Bungur Sari V no 166, Bandung',
        	'Tanggal_lahir' => '1983-03-18',
            'Tanggal_bergabung' => '2006-09-06',
            'created_at'=> now()
        ]);
        DB::table('karyawan')->insert([
        	'Nomor_induk' => 'IP06005',
        	'Nama' => 'Maulana',
        	'Alamat' => 'Jln Candi Agung no 7 Gg 5, Jakarta',
        	'Tanggal_lahir' => '1978-11-10',
            'Tanggal_bergabung' => '2006-09-10',
            'created_at'=> now()
        ]);
        DB::table('karyawan')->insert([
        	'Nomor_induk' => 'IP06006',
        	'Nama' => 'Agfika',
        	'Alamat' => 'Jln Nangka, Jakarta Timur',
        	'Tanggal_lahir' => '1979-02-07',
            'Tanggal_bergabung' => '2007-01-02',
            'created_at'=> now()
        ]);

        DB::table('karyawan')->insert([
        	'Nomor_induk' => 'IP06007',
        	'Nama' => 'James',
        	'Alamat' => 'Jln Merpati 8, Surabaya',
        	'Tanggal_lahir' => '1989-03-18',
            'Tanggal_bergabung' => '2007-03-04',
            'created_at'=> now()
        ]);

        DB::table('karyawan')->insert([
        	'Nomor_induk' => 'IP06008',
        	'Nama' => 'Octavanus',
        	'Alamat' => 'Jln A Yani 17 B 08, Sidoarjo',
        	'Tanggal_lahir' => '1989-03-18',
            'Tanggal_bergabung' => '2007-04-04',
            'created_at'=> now()
        ]);


        DB::table('karyawan')->insert([
        	'Nomor_induk' => 'IP06009',
        	'Nama' => 'Nugroho',
        	'Alamat' => 'Jln Duren tiga 167, Jakarta Selatan',
        	'Tanggal_lahir' => '1984-01-01',
            'Tanggal_bergabung' => '2008-01-16',
            'created_at'=> now()
        ]);
        DB::table('karyawan')->insert([
        	'Nomor_induk' => 'IP06010',
        	'Nama' => 'Raisa',
        	'Alamat' => 'Jln Kelapa Sawit, Jakarta Selatan',
        	'Tanggal_lahir' => '1990-12-17',
            'Tanggal_bergabung' => '2008-08-16',
            'created_at'=> now()
        ]);
    }
}
