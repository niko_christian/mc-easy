<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'niko',
        	'email' => 'nikochristian760@gmail.com',
            'email_verified_at'=>now(),
            'created_at'=> now(),
            'updated_at'=>now(),
            'is_admin'=>1,
            'password' => Hash::make(323576)
        ]);
       
    }
}
