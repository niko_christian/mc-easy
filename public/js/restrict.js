var ta = document.getElementById("restrict");
ta.addEventListener(
    'keypress',
    function (e) 
    {
        // Test for the key codes you want to filter out.
        if(e.keyCode==44)
        {
            document.getElementById("restrict").value += ".";
            e.preventDefault();
        }
        else if (! ((e.keyCode>=48&&e.keyCode<=57) || (e.keyCode>=65&&e.keyCode<=90) || (e.keyCode>=97&&e.keyCode<=122) || (e.keyCode==32||(e.keyCode==46)||(e.keyCode==45)) )) 
        {
            alert('Input yang diperbolehkan hanya A-Z dan 0-9');
            // Prevent the default event action (adding the
            // character to the textarea).
            e.preventDefault();
        }
    }
);

function showPassword(){
    var password = document.getElementById("showPassword");
    if (password.type == "password"){
        password.type = "text";
    }
    else{
        password.type = "password";
    }
}