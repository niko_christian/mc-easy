@extends('layouts.dashboard')


@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Edit Data Karyawan</h1>
</div>

<div class="container">


    @foreach($data as $e)
    <form method="post" action="/admin/karyawan/update/{{ $e->id }}" enctype="multipart/form-data">

        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="form-group">
            <label>Nomor_induk</label>
            {{ $e->Nomor_induk }}
            <input name="Nomor_induk" class="form-control" type="hidden"
                placeholder="Nomor_induk .." value="{{ $e->Nomor_induk }}">

            @if($errors->has('Nomor_induk'))
            <div class="text-danger">
                {{ $errors->first('Nomor_induk')}}
            </div>
            @endif
            
        </div>  
        <div class="form-group">
            <label>Nama</label>
            <input name="Nama" class="form-control"
                placeholder="Nama .." value="{{ $e->Nama }}">

            @if($errors->has('Nama'))
            <div class="text-danger">
                {{ $errors->first('Nama')}}
            </div>
            @endif
            
        </div>    
        <div class="form-group">
            <label>Alamat</label>
            <textarea name="Alamat" class="form-control"
                placeholder="Alamat .." value="{{ $e->Alamat }}">{{ $e->Alamat }}</textarea>
            @if($errors->has('Alamat'))
            <div class="text-danger">
                {{ $errors->first('Alamat')}}
            </div>
            @endif
            
        </div>  

        <div class="form-group">
            <label>Tanggal_lahir</label>
            <input name="Tanggal_lahir" class="form-control" type=date
                placeholder="Tanggal_lahir .." value="{{ $e->Tanggal_lahir }}">

            @if($errors->has('Tanggal_lahir'))
            <div class="text-danger">
                {{ $errors->first('Tanggal_lahir')}}
            </div>
            @endif
            
        </div>  

        <div class="form-group">
            <label>Tanggal_bergabung</label>
            <input name="Tanggal_bergabung" class="form-control" type=date
                placeholder="Tanggal_bergabung .." value="{{ $e->Tanggal_bergabung }}">

            @if($errors->has('Tanggal_bergabung'))
            <div class="text-danger">
                {{ $errors->first('Tanggal_bergabung')}}
            </div>
            @endif
            
        </div>  


        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Simpan">
        </div>

    </form>
    @endforeach

</div>
@endsection