@extends('layouts.dashboard')

@section('content')

<div class="container">

<h1 class="h3 mb-0 text-gray-800">Tambah Karyawan</h1></br>
    <form method="post" action="/admin/karyawan/store" enctype="multipart/form-data">

        {{ csrf_field() }}
       
        <div class="form-group">
            <label>Nomor_induk = </label>
            {{ $Nomor_induk}}
            <input type="hidden" name="Nomor_induk" class="form-control" placeholder="Judul .." value="{{ $Nomor_induk}}">

            @if($errors->has('Nomor_induk'))
            <div class="text-danger">
                {{ $errors->first('Nomor_induk')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <label>Nama</label>
            <input id="Nama" name="Nama" rows="4" cols="50" class="form-control" placeholder="Nama ..">
            @if($errors->has('Nama'))
            <div class="text-danger">
                {{ $errors->first('Nama')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>Alamat</label>
            <textarea name="Alamat" class="form-control"
                placeholder="Alamat .."> </textarea>

            @if($errors->has('Alamat'))
            <div class="text-danger">
                {{ $errors->first('Alamat')}}
            </div>
            @endif
            
        </div>    

        <div class="form-group">
            <label>Tanggal_lahir</label>
            <input name="Tanggal_lahir" class="form-control" type=date
                placeholder="Tanggal_lahir ..">

            @if($errors->has('Tanggal_lahir'))
            <div class="text-danger">
                {{ $errors->first('Tanggal_lahir')}}
            </div>
            @endif
            
        </div>  

        <div class="form-group">
            <label>Tanggal_bergabung</label>
            <input name="Tanggal_bergabung" class="form-control" type=date
                placeholder="Tanggal_bergabung ..">

            @if($errors->has('Tanggal_bergabung'))
            <div class="text-danger">
                {{ $errors->first('Tanggal_bergabung')}}
            </div>
            @endif
            
        </div>  

       

        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Simpan">
        </div>


    </form>

</div>

@endsection