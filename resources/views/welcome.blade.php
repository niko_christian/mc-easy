<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<!-- <link rel="icon" href="assets/img/favicon.ico"> -->
<title>Mc Easy</title>
<!-- Bootstrap core CSS -->
<link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
<!-- Fonts -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Righteous" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="{{ asset('/css/mediumish.css') }}" rel="stylesheet">
<!-- <link href="assets/css/style.css" rel="stylesheet"> -->
</head>
<body>

<!-- Begin Nav
================================================== -->
<nav class="navbar navbar-toggleable-sm navbar-light bg-white fixed-top mediumnavigation">
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"><i style="color: white" class="fas fa-bars"></i></span>
	</button>
	<div class="container">
		<div class="collapse navbar-collapse" id="navbarsExampleDefault">
			<!-- Begin Menu -->
			<ul class="navbar-nav ml-auto">
				
				<li class="nav-item">
					<a class="nav-link" href="#about">About</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#contact">Contact</a>
				</li>
				@if ( isset($datauser))
				<li class="nav-item">
					<a class="nav-link" href="/home">Dashboard</a>
				</li>				
				@else
				<li class="nav-item">
					<a class="nav-link" href="/login">Login</a>
				</li>				
				@endif															
								
			</ul>
			<!-- End Menu -->
			<!-- Begin Search -->		
			<!-- End Search -->
		</div>
	</div>
</nav>



	
	<div class="row about text-center" id="about">
		<div class="col-12">
			<h1 class="display-4 font-sec">About</h1>
			
		</div>
		<hr>
		<div class="col-12">
		<p> Program Software Cuti McEasy.</p>
		<div>
		<div class="col-12">
				<img class="coba-img" src="{{ asset('/img/bg.jpg') }}" alt=""  width="50%" height="30%">
		</div>

	</div>

	<div class="row contact text-center" id="contact">
		<div class="col-12">
			<h1 class="display-4 font-sec">Contact</h1>
		</div>
		<hr>
		<div class="col-12">
			<div class="d-flex flex-column align-items-center">
				<div class="d-flex justify-content-between w-50">
					<div class="h3 font-contact">
						<i class="fas fa-envelope mr-3"></i>Email : 
					</div>
					<div class="h3 font-contact">
						nikochristian760@gmail.com
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Begin Footer
	================================================== -->
	<div class="footer">
		<p class="pull-left">
			 Copyright &copy; 2021 McEasy
		</p>		
		<div class="clearfix">
		</div>
	</div>
	<!-- End Footer
	================================================== -->

</div>
<!-- /.container -->

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ asset('/js/jquery.min.js') }}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script> -->
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<!-- <script src="assets/js/ie10-viewport-bug-workaround.js"></script> -->
</body>
</html>
