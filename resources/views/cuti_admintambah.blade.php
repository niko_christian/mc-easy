@extends('layouts.dashboard')

@section('content')

<div class="container">

<h1 class="h3 mb-0 text-gray-800">Tambah Cuti</h1></br>
    <form method="post" action="/admin/cuti/store" enctype="multipart/form-data">

        {{ csrf_field() }}
       
        <div class="form-group">
            <label>Nomor_induk = </label>
            {{ $Nomor_induk}}
            <input type="hidden" name="Nomor_induk" class="form-control" placeholder="Judul .." value="{{ $Nomor_induk}}">

            @if($errors->has('Nomor_induk'))
            <div class="text-danger">
                {{ $errors->first('Nomor_induk')}}
            </div>
            @endif

        </div>
        <div class="form-group">
            <label>Tanggal_cuti</label>
            <input name="Tanggal_cuti" class="form-control" type=date
                placeholder="Tanggal_cuti ..">

            @if($errors->has('Tanggal_cuti'))
            <div class="text-danger">
                {{ $errors->first('Tanggal_cuti')}}
            </div>
            @endif
            
        </div>  
       
        <div class="form-group">
        <label for="Lama_cuti">Pilih Lama_cuti:</label>
        <select name="Lama_cuti" id="Lama_cuti">
            <?php 

            for($i=1; $i<=$Jumlah; $i++)
            {

                echo "<option value=".$i.">".$i."</option>";
            }
            ?> 
        </select>

            @if($errors->has('Lama_cuti'))
            <div class="text-danger">
                {{ $errors->first('Lama_cuti')}}
            </div>
            @endif
            
        </div>  
        <div class="form-group">
            <label>Keterangan</label>
            <textarea id="Keterangan" name="Keterangan" rows="4" cols="50" class="form-control" placeholder="Keterangan .."></textarea>
            @if($errors->has('Keterangan'))
            <div class="text-danger">
                {{ $errors->first('Keterangan')}}
            </div>
            @endif
        </div>

       

        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Simpan">
        </div>


    </form>

</div>

@endsection