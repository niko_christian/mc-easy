@extends('layouts.dashboard')


@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Edit Data Cuti</h1>
</div>

<div class="container">


    <form method="post" action="/admin/cuti/update/{{ $data->id }}" enctype="multipart/form-data">

        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="form-group">
            <label>Nomor_induk = </label>
            {{$data->Nomor_induk}}
            <input type="hidden" name="Nomor_induk" class="form-control" placeholder="Judul .." value="{{ $data->Nomor_induk }}">

            @if($errors->has('Nomor_induk'))
            <div class="text-danger">
                {{ $errors->first('Nomor_induk')}}
            </div>
            @endif

        </div>
        <div class="form-group">
            <label>Tanggal_cuti</label>
            <input name="Tanggal_cuti" class="form-control" type=date
                placeholder="Tanggal_cuti .."  value="{{$data->Tanggal_cuti }}">

            @if($errors->has('Tanggal_cuti'))
            <div class="text-danger">
                {{ $errors->first('Tanggal_cuti')}}
            </div>
            @endif
            
        </div>  
      
       
        <div class="form-group">
        <label for="Lama_cuti">Pilih Lama_cuti:</label>
        <select name="Lama_cuti" id="Lama_cuti">
        <option selected="selected">  {{$data->Lama_cuti}}</option>
            <?php 

            for($i=1; $i<=$data->Jumlah; $i++)
            {

                echo "<option value=".$i.">".$i."</option>";
            }
            ?> 
        </select>

            @if($errors->has('Lama_cuti'))
            <div class="text-danger">
                {{ $errors->first('Lama_cuti')}}
            </div>
            @endif
            
        </div>  
        <div class="form-group">
            <label>Keterangan</label>
            <textarea id="Keterangan" name="Keterangan" rows="4" cols="50" class="form-control" placeholder="Keterangan ..">{{$data->Keterangan }}</textarea>
            @if($errors->has('Keterangan'))
            <div class="text-danger">
                {{ $errors->first('Keterangan')}}
            </div>
            @endif
        </div>


        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Simpan">
        </div>

    </form>


</div>
@endsection