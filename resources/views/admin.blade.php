@extends('layouts.dashboard')

@section('content')

<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard </h1>
    <h4 class="h5 mb-0 text-gray-800">Just Click to go to dashboard</h4>
</div>


<!-- Content Row -->
<div class="row">

    <!-- karyawan -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2" onclick="location.href='/admin/karyawan'">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col">
                        <div class="font-weight-bold text-warning text-uppercase mb-1">
                            Karyawan
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                           Untuk Atur Karyawan
                        </div>
                        <a href="/admin/karyawan" class="btn btn-warning btn-icon-split btn-block mt-4 mb-0">
                            <span class="text">lihat dashboard</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2" onclick="location.href='/admin/cuti'">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col">
                        <div class="font-weight-bold text-danger text-uppercase mb-1">
                            Cuti
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                           Untuk Atur Cuti
                        </div>
                        <a href="/admin/cuti" class="btn btn-danger btn-icon-split btn-block mt-4 mb-0">
                            <span class="text">lihat dashboard</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
   

    
     


@endsection