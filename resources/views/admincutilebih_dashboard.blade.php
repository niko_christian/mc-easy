@extends('layouts.dashboard')

@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List Cuti Lebih dari 1 X</h1>
</div>


<div class="table-responsive">
    <table class="table table-bordered table-striped display nowrap" style="width:100%" id="dataTable">
        <thead>
            <tr>
                <th>Nomor_induk</th>
                <th>Nama</th>
                <th>Tanggal_cuti</th>
                <th>Lama_cuti</th>
                <th>Keterangan</th>


            </tr>
        </thead>
        <tbody>
            @foreach($data as $e)
            <tr>
                <td>{{ $e->Nomor_induk }}</td>
                <td>{{ $e->Nama }}</td>
                <td>{{ $e->Tanggal_cuti}}</td>

                <td>{{ $e->Lama_cuti}}</td>

                <td>{{ $e->Keterangan}}</td>
             

            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection