@extends('layouts.dashboard')

@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List Karyawan</h1>
</div>


<div class="table-responsive">
    <table class="table table-bordered table-striped display nowrap" style="width:100%" id="dataTable">
        <thead>
            <tr>
                <th>No</th>
                <th>Nomor_induk</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Tanggal_lahir</th>
                <th>Tanggal_bergabung</th>
                <th>OPSI</th>

            </tr>
        </thead>
        <tbody>
            @foreach($data as $e)
            <tr>
                <td>{{ $e->id }}</td>
                <td>{{ $e->Nomor_induk }}</td>
                <td>{{  $e->Nama  }}</td>
                <td>{{ $e->Alamat}}</td>
                <td>{{ $e->Tanggal_lahir}}</td>
                <td>{{ $e->Tanggal_bergabung }}</td>
 

                <td>
                    <a href="/admin/karyawan/edit/{{ $e->id }}" class="btn btn-warning">Edit</a>
                    <a href="/admin/karyawan/hapus/{{ $e->id }}" class="btn btn-danger">Hapus</a>
                
                </td>

            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection