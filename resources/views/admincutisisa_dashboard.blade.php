@extends('layouts.dashboard')

@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List Karyawan Cuti</h1>
</div >
@if($error==1)
<div style="color:red";>Tidak bisa menambahkan Cuti</div>
@endif
<div class="table-responsive">
    <table class="table table-bordered table-striped display nowrap" style="width:100%" id="dataTable">
        <thead>
            <tr>
                <th>Nomor_induk</th>
                <th>Nama</th>
                <th>Sisa_cuti (Hari)</th>
                <th>OPSI</th>

            </tr>
        </thead>
        <tbody>
            @foreach($user as $e)
            <tr>
                <td>{{ $e->Nomor_induk }}</td>
                <td>{{ $e->Nama }}</td>
                <td>{{ $e->jumlah }}</td>
 


                <td>
                    <a href="/admin/cuti/tambah/{{ $e->Nomor_induk }}" class="btn btn-success">Tambah</a>
                
                </td>

            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection