@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('PESAN') }}</div>
                <br>
                <p style="padding:10px">Silahkan Menghubungi Admin</p>
            </div>
        </div>
    </div>
</div>
@endsection
