<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        
        if ($user->is_admin) {
            return redirect('/admin');
        }

    }

    public function landingpage(){
        $datauser = Auth::user();                
        // echo $databerita;
        if($datauser){
            return view('welcome', ['datauser' => $datauser]);            
        }
        else{
            return view('welcome');
        }                        
    }

}
