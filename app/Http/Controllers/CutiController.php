<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use DateTime;
use DateTimeZone;
use App\Cuti;
use auth;


class CutiController extends Controller
{
    public function index(){                
        $data= DB::select("SELECT cuti.id, karyawan.Nama, cuti.Tanggal_cuti, cuti.Lama_cuti, cuti.Nomor_induk, cuti.Keterangan FROM cuti INNER JOIN karyawan ON cuti.Nomor_induk = karyawan.Nomor_induk");
        //var_dump($data[0]);
     
        return view('admincuti_dashboard', ['data' => $data]);
    }

    public function sisa($e){              
        $pelajaran = ["Algoritma & Pemrograman","Kalkulus","Pemrograman Web"];  
        $data= DB::select("SELECT karyawan.Nomor_induk from karyawan");
        if(empty($data)){
            $user=[];
        }
        foreach ($data as $value) {
           // echo $value->Nomor_induk; gettype($u);
            $noinduk= $value->Nomor_induk;
            // echo $value->Nama;
            // var_dump($value);
            //echo(gettype($value));
            $durasi= DB::select("SELECT cuti.Lama_cuti FROM cuti INNER JOIN karyawan ON cuti.Nomor_induk = karyawan.Nomor_induk WHERE cuti.Nomor_induk='".$noinduk."'");
            $jumlah =0;
            foreach($durasi as $nilai)
            {
                $jumlah = $jumlah+$nilai->Lama_cuti;
            }
            $durasi= DB::select("SELECT karyawan.Nama FROM karyawan WHERE karyawan.Nomor_induk='".$noinduk."'");
            foreach($durasi as $nilai)
            {
                $Name = $nilai->Nama;
            }
            // echo $value->Nomor_induk;
            // echo " ".$jumlah;
            // echo "<br>";
            $object = new \stdClass();
            $object->Nomor_induk = $value->Nomor_induk;
            $object->Nama = $Name;
            $object->jumlah = 12-$jumlah;
            $user[] = $object;
         
          }
        //   $object = new \stdClass();
        //   $object->property = 'Here we go';
        //   $object->p = 'FGH';
        //   $user[] = $object;
        //   $object = new \stdClass();
        //   $object->property = 'GO we';
        //   $object->p = 'ABC';
        //   $user[] = $object;
          
        // foreach ($user as $value){
        //    echo $value->Nomor_induk;
        //    echo "  ".$value->Nama;
        //    echo "  ".$value->jumlah;
        //    echo "<br>";
        // }
    
        //echo 'a';
        return view('admincutisisa_dashboard', ['user' => $user,'error'=>$e]);
    }

    public function lebih(){              
        $pelajaran = ["Algoritma & Pemrograman","Kalkulus","Pemrograman Web"];  
        $count= DB::select("SELECT Nomor_induk FROM cuti
                            GROUP BY Nomor_induk HAVING COUNT(Nomor_induk) > 1;");
        if(empty($count)){
            $data=[];
        }
        
        foreach ($count as $value) {
           // echo $value->Nomor_induk; gettype($u);
            $noinduk= $value->Nomor_induk;
            //echo $noinduk;
            // echo $value->Nama;
            // var_dump($value);
            //echo(gettype($value));
            $durasi= DB::select("SELECT cuti.id, karyawan.Nama, cuti.Tanggal_cuti, cuti.Lama_cuti, cuti.Nomor_induk, cuti.Keterangan FROM cuti INNER JOIN karyawan ON cuti.Nomor_induk = karyawan.Nomor_induk WHERE cuti.Nomor_induk='".$noinduk."'");
            foreach($durasi as $nilai)
            {
                $data[] = $nilai; 
            }
         
          }
          
          //var_dump($data);
          return view('admincutilebih_dashboard', ['data' => $data]);
    }
    public function cutitambah($Nomor_induk)
    {   

        //echo $Nomor_induk;
        $durasi= DB::select("SELECT cuti.Lama_cuti FROM cuti WHERE cuti.Nomor_induk='".$Nomor_induk."'");
        $jumlah =0;
        foreach($durasi as $nilai)
        {
            $jumlah = $jumlah+$nilai->Lama_cuti;
        }
        //echo $jumlah;
        if(12-$jumlah>0){
            return view('cuti_admintambah',['Nomor_induk' => $Nomor_induk,'Jumlah'=>12-$jumlah]);

        }
        else
        {
            $e = 1;
            return redirect('/admin/sisacuti/'.$e);
        }
       
    }
    public function cutistore(Request $request)
    {
        //echo 'a';
        $this->validate($request, [
            'Nomor_induk' => 'required',
            'Tanggal_cuti' => 'required',
            'Lama_cuti' => 'required',
            'Keterangan' => 'required',


        ]);

        Cuti::create([
            'Nomor_induk' => $request->Nomor_induk,
            'Tanggal_cuti' => $request->Tanggal_cuti,
            'Lama_cuti' => $request->Lama_cuti,
            'Keterangan' => $request->Keterangan

        ]);
        return redirect('/admin/cuti');
    }
    public function cutiedit($idcuti)
    {
        //echo $idcuti;
        $data = DB::table('cuti')->where('id',$idcuti)->get();
        foreach($data as $nilai){
            $Nomor_induk=$nilai->Nomor_induk;
            $Lama_cuti=$nilai->Lama_cuti;
            $Tanggal_cuti=$nilai->Tanggal_cuti;
            $Keterangan=$nilai->Keterangan;
            $id=$nilai->id;

        }
        $durasi= DB::select("SELECT cuti.Lama_cuti FROM cuti INNER JOIN karyawan ON cuti.Nomor_induk = karyawan.Nomor_induk WHERE cuti.Nomor_induk='".$Nomor_induk."'");
        $jumlah =0;
        foreach($durasi as $nilai)
        {
            $jumlah = $jumlah+$nilai->Lama_cuti;
        }
        // echo 12-$jumlah+$Lama_cuti;
        // echo ' '.$Nomor_induk;
        // echo ' '.$Lama_cuti;
        $data = new \stdClass();
        $data->Nomor_induk = $Nomor_induk;
        $data->Lama_cuti = $Lama_cuti;
        $data->Tanggal_cuti = $Tanggal_cuti;
        $data->Jumlah = 12-$jumlah+$Lama_cuti;
        $data->Keterangan = $Keterangan;
        $data->id = $id;
        //var_dump($data);
        return view('cuti_adminedit', ['data' => $data ]);
    }
   
    public function cutiupdate($idcuti, Request $request)
    {
        $this->validate($request, [
            'Nomor_induk' => 'required',
            'Tanggal_cuti' => 'required',
            'Lama_cuti' => 'required',
            'Keterangan' => 'required',


        ]);
        
        DB::table('cuti')->where('id',$idcuti)->update([
            'Nomor_induk' => $request->Nomor_induk,
            'Tanggal_cuti' => $request->Tanggal_cuti,
            'Lama_cuti' => $request->Lama_cuti,
            'Keterangan' => $request->Keterangan,
            ]);
        return redirect('/admin/cuti');
    }
    public function cutidelete($idcuti)
    {

        DB::table('cuti')->where('id',$idcuti)->delete();
        return redirect('/admin/cuti');
    }
    
}
