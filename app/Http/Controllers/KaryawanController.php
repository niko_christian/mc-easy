<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use DateTime;
use DateTimeZone;
use App\Karyawan;
use auth;


class KaryawanController extends Controller
{
    public function index(){                
        $data = Karyawan::get();
        return view('adminkaryawan_dashboard', ['data' => $data]);
    }

    public function karyawantambah()
    {   
        $data = DB::table('karyawan')->latest('id')->first();;
        $nomor= $data->id;
        
        //echo $nomor+1;
        $nomor=$nomor+1;
        if($nomor<10){
                $nomor=(string)$nomor;
                //echo (gettype($nomor));
                $Nomor_induk="IP0600".$nomor;
        }
        else if($nomor<100){
            $nomor=(string)$nomor;
            //echo (gettype($nomor));
            $Nomor_induk="IP060".$nomor;
        }
        else if($nomor<1000){
            $nomor=(string)$nomor;
            //echo (gettype($nomor));
            $Nomor_induk="IP06".$nomor;
        }
        //echo $Nomor_induk;
    
        

        return view('karyawan_admintambah',['Nomor_induk' => $Nomor_induk]);
    }
    public function karyawanstore(Request $request)
    {
        //echo 'a';
        $this->validate($request, [
            'Nomor_induk' => 'required',
            'Nama' => 'required',
            'Alamat' => 'required',
            'Tanggal_lahir' => 'required',
            'Tanggal_bergabung' => 'required'
            

        ]);

        Karyawan::create([
            'Nomor_induk' => $request->Nomor_induk,
            'Nama' => $request->Nama,
            'Alamat' => $request->Alamat,
            'Tanggal_lahir' => $request->Tanggal_lahir,
            'Tanggal_bergabung' => $request->Tanggal_bergabung,
            
        ]);
        return redirect('/admin/karyawan');
    }
    public function karyawanedit($idkaryawan)
    {
        //echo $idkaryawan;
        $data = DB::table('karyawan')->where('id',$idkaryawan)->get();
        return view('karyawan_adminedit', ['data' => $data ]);
    }
    public function karyawanupdate($idkaryawan, Request $request)
    {
        $this->validate($request, [
            'Nomor_induk' => 'required',
            'Nama' => 'required',
            'Alamat' => 'required',
            'Tanggal_lahir' => 'required',
            'Tanggal_bergabung' => 'required'
        ]);
        echo $idkaryawan;
        echo $request->Nomor_induk;
        echo $request->Nama;
        echo $request->Alamat;
        echo $request->Tanggal_lahir;
        echo $request->Tanggal_bergabung;
        
        
        DB::table('karyawan')->where('id',$idkaryawan)->update([
            'Nomor_induk' => $request->Nomor_induk,
            'Nama' => $request->Nama,
            'Alamat' => $request->Alamat,
            'Tanggal_lahir' => $request->Tanggal_lahir,
            'Tanggal_bergabung' => $request->Tanggal_bergabung
            ]);
        return redirect('/admin/karyawan');
    }
    public function karyawandelete($idkaryawan)
    {
        $durasi= DB::select("SELECT karyawan.Nomor_induk FROM karyawan WHERE karyawan.id='".$idkaryawan."'");
        var_dump($durasi);
        foreach($durasi as $nilai)
            {
                $Nomor_induk = $nilai->Nomor_induk;
            }
        DB::table('cuti')->where('Nomor_induk',$Nomor_induk)->delete();
        DB::table('karyawan')->where('id',$idkaryawan)->delete();
        return redirect('/admin/karyawan');
    }
    
}
