<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuti extends Model
{
    protected $table = "cuti";
    // public function karyawan(){
    // 	return $this->belongsTo('App\Karyawan');
    // }
    protected $casts = [
        'id' => 'integer',
        'Lama_cuti' => 'integer'
    ];
    protected $fillable = ['Nomor_induk','Tanggal_cuti','Lama_cuti', 'Keterangan'];
  
}