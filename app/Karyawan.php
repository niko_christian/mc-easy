<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $table = "karyawan";
    public function cuti(){
    	return $this->hasMany('App\Cuti');
    }
    protected $casts = [
        'id' => 'integer'
    ];
    protected $fillable = ['Nomor_induk','Nama','Alamat', 'Tanggal_lahir','Tanggal_bergabung'];
  
}